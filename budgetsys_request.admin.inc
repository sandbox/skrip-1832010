<?php
/**
 * @file
 * This file contains functions & forms to administer the Budget Request System
 */

/**
 * Form builder; Configure user settings for this site.
 *
 */
function budgetsys_request_admin_settings() {
  $form = array();
    // Select the final value type that is displayed for past years
  $form['budgetsys_request_request_type'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('budgetsys_request_request_type',''),
    '#title' => t('Budget Request Value Type'),
    '#required' => TRUE,
    '#options' => budgetsys_value_types_array(), 
    '#description' => t('Please select the Budget Value type to be used for Budget Requests'),
  ); 
  $form['budgetsys_request_new_account_number_prefix'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('budgetsys_request_new_account_number_prefix', 'N'),
    '#title' => t('New Account Number Prefix'),
    '#required' => TRUE,
    '#description' => t('This value will be preprended to newly created account numbers.'),
  );
  $form['budgetsys_request_new_account_number_generator'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('budgetsys_request_new_account_number_generator', '100'),
    '#title' => t('New Account Number Start'),
    '#required' => TRUE,
    '#description' => t('This is the starting number used when generating new account numbers. Changing this will only affect newly created accounts.'),
  );
  $form['#validate'][] = 'budgetsys_request_admin_settings_validate';  
  return system_settings_form($form);
}

function budgetsys_request_admin_settings_validate($form, &$form_state) {
  $value = $form_state['values']; 

  if(!is_numeric($value['budgetsys_request_new_account_number_generator'])) {
    form_set_error('budgetsys_request_new_account_number_generator', 'The New Account Number Start must be an integer.');
  }
}
