<?php
/******************************************************************
**********************Budget Request Entity************************ 
*******************************************************************/
function budgetsys_request_add() {
  global $user;

  $budget_request = entity_create('budgetsys_request', array('type' => 'budgetsys_request'));
  drupal_set_title(t('Create Budget Request'));
  return drupal_get_form('budgetsys_request_form', $budget_request);
  
}
/**
 * Budget System Request edit form
 */
function budgetsys_request_page_edit ($budget_request) {
    drupal_set_title(t('<em>Edit Budget Request: @title</em>', array('@title' => $budget_request->title)), PASS_THROUGH);
    return drupal_get_form('budgetsys_request_form', $budget_request);
}
/**
 * Implements hook_form().
 */
function budgetsys_request_form($form, &$form_state, $budget_request, $budget_org = NULL) {
  $form['#id'] = 'budgetsys-request-form';
  $form['#budget_request'] = $budget_request;
  $form_state['budget_request'] = $budget_request;
  
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($budget_request->title) ? $budget_request->title : '',
    '#weight' => -5,
    '#required' => TRUE,
  );
 $organization = budgetsys_org_load($budget_request->oid);
if($organization) {
 $form['display_organization'] = array(
    '#markup' => $organization->title,
    '#suffix' => '<br>',
    '#prefix' => '<b>Organization</b>: ',
    '#weight' => -10,                        
  ); 
} 

  $current_year = variable_get('budgetsys_current_fiscal_year_taxonomy', '');
  $term = taxonomy_term_load($current_year);
  $form['year_display'] = array(
    '#markup' => $term->name,
    '#prefix' => t('<b>Year</b>: '),
    '#suffix' => '<br>',
    '#weight' => -9,
  ); 
  $form['year'] = array(
    '#type' => 'value',
    '#value' => $current_year,
  );
  $form['oid'] = array(
    '#type' => 'value',
    '#value' => $budget_request->oid,
  );
  $form['revision'] = array(
    '#access' => user_access('administer budget requests'),
    '#type' => 'checkbox',
    '#title' => t('Create new revision'),
    '#default_value' => 1,
  );
  field_attach_form('budgetsys_request', $budget_request, $form, $form_state);
  
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('budgetsys_request_form_submit'),
  );
 if (!empty($budget_request->brid)) {
    $form['buttons']['delete'] = array(
      '#access' => budgetsys_request_access('delete', $budget_request),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('budgetsys_request_form_delete_submit'),
    );
  }  
  $form['#validate'][] = 'budgetsys_request_form_validate';
  
 return $form;
} 

/**
 * Implements hook_form_validate().
 */
function budgetsys_request_form_validate($form, &$form_state) {
  $budget_request = $form_state['budget_request'];
  
  if($form_state['values']['og_group_ref']['und'][0]['target_id']) {
    $oid = $form_state['values']['og_group_ref']['und'][0]['target_id'];
  } else {
    $oid = $form_state['values']['oid'];
  }

  $budget_org = budgetsys_org_load($oid);
  if(!property_exists($budget_request, 'brid') && budgetsys_request_load_org_request($budget_org)) {
     form_set_error('oid', 'A budget request already exists for this organization for the current year.');
  }
  field_attach_form_validate('budgetsys_request', $budget_request, $form, $form_state); 
}

/**
 * Implements hook_form_submit().
 */
function budgetsys_request_form_submit($form, &$form_state) {
  global $user;
  $budget_request = &$form_state['budget_request'];
  
  if (empty($budget_request->uid)) {
    $budget_request->uid = $user->uid;
  }
  $budget_request->title = $form_state['values']['title'];
  $budget_request->revision = $form_state['values']['revision'];
  $budget_request->oid = $form_state['values']['oid'];
  $budget_request->year = $form_state['values']['year'];
  if($form_state['values']['og_group_ref']['und'][0]['target_id']) {
    $budget_request->oid = $form_state['values']['og_group_ref']['und'][0]['target_id'];
  }
  
  field_attach_submit('budgetsys_request', $budget_request, $form, $form_state);
  budgetsys_request_save($budget_request);
  
  drupal_set_message(t('Budget Request saved.'));
  $form_state['redirect'] = 'budget/request/' . $budget_request->brid;
  
}

/**
 * Delete a Budget Request submit function
 */
function budgetsys_request_form_delete_submit ($form, &$form_state) {
    $destination = array();
    if (isset($_GET['destination'])) {
        $destination = drupal_get_destination();
        unset($_GET['destination']);
    }
    $budget_request = $form['#budget_request'];
    $form_state['redirect'] = array('budget/request/' . $budget_request->brid . '/delete', array('query' => $destination));
}
/**
 * Budget Request Delete Confirm
 */
function budgetsys_request_delete_confirm($form, &$form_state, $budget_request) {
    $form['#budget_request'] = $budget_request;
    // Always provide entity id in the same form key as in the entity edit form.
    $form['brid'] = array('#type' => 'value', '#value' => $budget_request->brid);
    return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $budget_request->title)),
    'budget/request/' . $budget_request->brid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
    );
}
function budgetsys_request_delete_confirm_submit($form, &$form_state) {
    if ($form_state['values']['confirm']) {
        $budget_request = budgetsys_request_load($form_state['values']['brid']);
        budgetsys_request_delete($form_state['values']['brid']);
        watchdog('budgetsys_request', 'Budget Request: deleted %title.', array('%title' => $budget_request->title));
        drupal_set_message(t('Budget Request %title has been deleted.', array('%title' => $budget_request->title)));
    }
    $form_state['redirect'] = '<front>';
} 
 
function budgetsys_request_page_view($budget_request, $view_mode = 'full') {
   field_attach_prepare_view('budgetsys_request', array($budget_request->brid => $budget_request), $view_mode);
   entity_prepare_view('budgetsys_request', array($budget_request->brid => $budget_request));
   $fields = field_attach_view('budgetsys_request', $budget_request, $view_mode);
   $content = array(
    '#theme' => 'budgetsys_request',
    '#budget_request' => $budget_request,
    '#fields' => $fields,
    '#view_mode' => 'full',
   );
  return $content;
}

/******************************************************************
**********************Budget Request Values Form******************* 
*******************************************************************/
/**
 * Takes an organization and determines if it has a budget request submitted for the current year,
 * if so it displays the budget request dashboard, else it displays the add budget request form.
 * 
 * @param $budget_org
 * An Organization entity
 * 
 * @return
 * Either a dashboard or a budget requset creation form
 */
function budgetsys_request_organization_request($budget_org, $op = 'view') {
  $budget_request = budgetsys_request_load_org_request($budget_org);
  if($op == 'edit') {
    $budget_request_obj = budgetsys_request_load($budget_request['brid']);
    drupal_set_title('Edit Budget Request');
    return drupal_get_form('budgetsys_request_form', $budget_request_obj, $budget_org);
  }

  if($budget_request) {
    drupal_set_title('Budget Request Dashboard');
    $year = variable_get('budgetsys_current_fiscal_year_taxonomy');
    $path = "budget/request/$budget_request[brid]";
    $edit_path = "budget/org/$budget_org->oid/request/edit";
    $output = '';
    $output .= l('View Budget Request', $path);
    $output .= '<br>';
    $output .= l('Edit Budget Request', $edit_path);
    $output .= views_embed_view('budget_request_dashboard', 'budget_request_dashboard', $budget_org->oid, $year);
    return $output;
  }
  $path = "budget/org/$budget_org->oid/request/create";
  $options = array(
    'query' => array(
      'og_group_ref' => $budget_org->oid,
    ),
  );
  return drupal_goto($path, $options);
}

/**
 * The creation form for a Budget Request. This page is redirected to from the budget request dashboard
 */
function budgetsys_request_organization_request_create($budget_org) {
  $budget_request = entity_create('budgetsys_request', array('type' => 'budgetsys_request'));
  drupal_set_title('Budget Request');
  return drupal_get_form('budgetsys_request_form', $budget_request, $budget_org);
}
/**
 * Implements hook_form()
 */
function budgetsys_request_value_form($form, &$form_state, $budget_org, $value, $mode = 'new') {
  $form_state['value'] = $value;

  $current_year = variable_get('budgetsys_current_fiscal_year_taxonomy', '');
  $term = taxonomy_term_load($current_year);
  module_load_include('inc', 'budgetsys_value', 'budgetsys_value.pages');
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $value->type,
    );
  $form['display_type'] = array(
      '#markup' => $value->type,
      '#suffix' => '<br>',
      '#prefix' => '<b>Budget Value Type</b>: ',
      '#weight' => -12,
  );
  $form['year'] = array(
    '#type' => 'value',
    '#value' => $current_year,
     );
     $form['display_year'] = array(
    '#markup' => $term->name,
    '#suffix' => '<br>',
    '#prefix' => '<b>Fiscal year</b>: ',
    '#weight' => -9,
   );
  $form['oid'] = array(
            '#type' => 'value',
            '#value' => $budget_org->oid,
  );
  $form['display_oid'] = array(
    '#markup' => $budget_org->title,
    '#prefix' => t('<b>Organization</b>: '),
    '#suffix' => '<br>',
    '#weight' => -11,
  ); 
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $value->uid,
  );
  $form['account_title_display'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="account_title_display_wrapper">',
    '#suffix' => '</div>',
    '#weight' => -4,
  );  
  if($value->account_number) {
    $form['account_title_display']['#markup'] = t('<b>Account Title</b>: @account_title', array('@account_title' => $value->title));
  }
  $account_number = $value->account_number ? $value->account_number : NULL;
  $form['account_number'] = array(
    '#type' => 'select',
    '#description' => t('The account number to request funds for'),
    '#title' => t('Account'),
    '#default_value' => $account_number,
    '#options' => budgetsys_request_load_account_numbers($budget_org, $account_number),
    '#weight' => -3,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'budgetsys_request_account_title_callback',
      'wrapper' => 'account_title_display_wrapper',
      //'method' defaults to replaceWith, but valid values also include
      // append, prepend, before and after.
      // 'method' => 'replaceWith',
      // 'effect' defaults to none. Other valid values are 'fade' and 'slide'.
      // See ajax_example_autotextfields for an example of 'fade'.
      'effect' => 'fade',
      // 'speed' defaults to 'slow'. You can also use 'fast'
      // or a number of milliseconds for the animation to last.
      // 'speed' => 'slow',
      // Don't show any throbber...
      'progress' => array('type' => 'none'),
      'method' => 'replace',
    ),    
  );

  $form['value'] = array(
    '#type' => 'textfield',
    '#description' => 'Only use number values with no commas or a $',
    '#title' => t('Account Value'),
    '#default_value' => $value->value,
    '#required' => TRUE,
  );
  $form['revision'] = array(
    '#access' => user_access('administer budget values'),
    '#type' => 'checkbox',
    '#title' => t('Create new revision'),
    '#default_value' => 0,
  );
  field_attach_form('budgetsys_value', $value, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Funding Request'),
    '#submit' => $submit + array('budgetsys_request_value_form_submit'),
  );
  $value_id = entity_id('budgetsys_value' ,$value);
    if (!empty($value_id) && budgetsys_request_requested_access($budget_org, 'edit')) {
      $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('budgetsys_request_value_form_submit_delete'),
      );
    }

  $form['#validate'][] = 'budgetsys_request_value_form_validate';
    
  if((isset($form_state['values']['account_number']))) {
    $account_number = $form_state['values']['account_number'];
    $account_title = db_select('budgetsys_line', 'bl')
      ->fields('bl', array('title'))
      ->condition('account_number', $account_number)
      ->execute()
      ->fetchAssoc();
    $form['account_title_display']['#markup'] = t('<b>Account Title</b>: @account_title', array('@account_title' => $account_title['title']));
    $form['title'] = array(
      '#type' => 'value',
      '#value' => $account_title['title'],
    );
  }
  $form['budgetsys_account_year']['#type'] = 'hidden';
  $form['budgetsys_account_year']['und']['#required'] = FALSE;
 # dpm($form);
 
  return $form;          
}

function budgetsys_request_value_form_validate($form, &$form_state) {
  $value = $form_state['value'];  

  if(!is_numeric($form_state['values']['value'])) {
    form_set_error('value', 'The value needs to be numeric with no commas or dollar symbols.');
  }
    // Field validation.  
  field_attach_form_validate('budgetsys_value', $value, $form, $form_state);
}

function budgetsys_request_value_form_submit($form, &$form_state) {
  $lang = $form['budgetsys_account_year']['#language'];
  $args = arg();
  $value = $form_state['value'];

  entity_form_submit_build_entity('budgetsys_value', $value, $form, $form_state);

  $value->budgetsys_account_year[$lang][0]['tid'] = $value->year;

  budgetsys_value_save($value);
  
  $redir_path = "budget/org/$args[2]/request"; 
  $form_state['redirect'] = $redir_path;
  
  drupal_set_message(t('Value %title saved.', array('%title' => entity_label('budgetsys_value', $value))));  
}
function budgetsys_request_value_form_submit_delete($form, &$form_state) {

  $arg = arg();
  $value = $form_state['value'];
  $value_uri = entity_uri('budgetsys_value', $value);
  $path = "$arg[0]/$arg[1]/$arg[2]/$arg[3]/$arg[4]/delete";
  $form_state['redirect'] = $path;
}

/**
 * Delete confirmation form.
 */
function budgetsys_request_value_delete_form($form, &$form_state, $value) {
  $form_state['value'] = $value;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['value_type_id'] = array('#type' => 'value', '#value' => entity_id('budgetsys_value' ,$value));
  $value_uri = entity_uri('budgetsys_value', $value);
  return confirm_form($form,
    t('Are you sure you want to delete the requested value %title?', array('%title' => entity_label('budgetsys_value', $value))),
    $value_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function budgetsys_request_value_delete_form_submit($form, &$form_state) {
  $value = $form_state['value'];
  $arg = arg();
  budgetsys_value_delete($value);

  drupal_set_message(t('Request Value %title deleted.', array('%title' => entity_label('budgetsys_value', $value))));
  $redir_path = "budget/org/$arg[2]/request"; 
  $form_state['redirect'] = $redir_path;
}

/**
 * Builds a budget request value form
 * 
 * @param $budget_org
 * An Organization object
 * 
 * @return
 * The budget request value form
 */
function budgetsys_request_add_request_value($budget_org) {
  module_load_include('inc', 'budgetsys_value', 'budgetsys_value.pages');
  drupal_set_title('Request Account Funding');

  
  $request_type = variable_get('budgetsys_request_request_type');
  $value = entity_create('budgetsys_value', array('type' => $request_type));
  
  return drupal_get_form('budgetsys_request_value_form',$budget_org, $value);

}
/**
 * AJAX callback for the account number on the budget request value form
 */
function budgetsys_request_account_title_callback($form, $form_state) { 
  return $form['account_title_display'];
}


/******************************************************************
********************Budget Request Accounts Form******************* 
*******************************************************************/
/**
 * Page to select budget line Type to add new task.
 */
function budgetsys_request_account_admin_add_page($budget_org) {
  drupal_set_title('Request New Account');
  $oid = $budget_org->oid;
  $items = array();
  foreach (budgetsys_line_item_types() as $line_type_key => $line_type) {
    $items[] = l('Add ' . entity_label('budgetsys_line_type', $line_type) . ' Account', "budget/org/$oid/request/add/account/" . $line_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of budget account to create.')));
}
/**
 * Add a new budget line item (account).
 */
function budgetsys_request_account_add($budget_org, $type) {
  $line_type = budgetsys_line_item_types($type);

  $line_item = entity_create('budgetsys_line', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('budgetsys_line_type', $line_type))));

  $output = drupal_get_form('budgetsys_request_line_item_form', $budget_org, $line_item);

  return $output;
}

function budgetsys_request_generate_new_account() {
  $account_base = variable_get('budgetsys_request_new_account_number_generator','100');
  $new_account_nbr = $account_base + 1;
  variable_set('budgetsys_request_new_account_number_generator', $new_account_nbr);
  $prefix = variable_get('budgetsys_request_new_account_number_prefix','N');
  $new_account_nbr = $prefix . $new_account_nbr;
  return $new_account_nbr;
}
/**
 * Budget Line Form.
 */
function budgetsys_request_line_item_form($form, &$form_state, $budget_org, $line_item) {
  // Save the line item incase we need it later.
  $form['#line_item'] = $line_item;
  $form_state['line_item'] = $line_item;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Account Title'),
    '#default_value' => $line_item->title,
  );
$account_number = budgetsys_request_generate_new_account();
  $form['account_number'] = array(
    '#type' => 'value',
    '#default_value' => $account_number,
  );
  $form['account_number_display'] = array(
    '#markup' => $account_number,
    '#prefix' => '<b>Account Number</b>: ',
    '#suffix' => '<br>',
    '#weight' => -5,
  );  

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $line_item->uid,
  );

  $form['oid'] = array(
    '#type' => 'value',
    '#value' => $budget_org->oid,
  );
  $form['orginization_display'] = array(
    '#markup' => $budget_org->title,
    '#prefix' => '<b>Organization</b>: ',
    '#suffix' => '<br>',
    '#weight' => -4,
  );      
  $form['account_type_display'] = array(
    '#markup' => $line_item->type,
    '#prefix' => '<b>Account Type</b>: ',
    '#suffix' => '<br>',
    '#weight' => -7,
  );

  field_attach_form('budgetsys_line', $line_item, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Request New Account'),
    '#submit' => $submit + array('budgetsys_request_line_form_submit'),
  );

  $form['#validate'][] = 'budgetsys_request_line_form_validate';

  return $form;
}

function budgetsys_request_line_form_validate($form, &$form_state) {

}

/**
 * Budget Line submit handler.
 */
function budgetsys_request_line_form_submit($form, &$form_state) {
  $line_item = $form_state['line_item'];
  $arg = arg();
  
  entity_form_submit_build_entity('budgetsys_line', $line_item, $form, $form_state);

  budgetsys_line_save($line_item);

  $line_item_uri = entity_uri('budgetsys_line', $line_item);

  $redir_path = "budget/org/$arg[2]/request"; 
  $form_state['redirect'] = $redir_path;

  drupal_set_message(t('Account %title requested.', array('%title' => entity_label('budgetsys_line', $line_item))));
}