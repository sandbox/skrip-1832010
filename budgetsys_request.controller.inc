<?php
/**
 * @file
 * The Budget System Request class.
 */
   /**
   * Budget System Request class.
   */
  class BudgetsysRequestClass extends Entity {
    protected function defaultLabel() {
      return $this->title;
    }
    
    protected function defaultUri() {
      return array('path' => 'budget/request/' . $this->identifier());
    }
  }
  
  class BudgetsysRequestClassController extends EntityAPIController {
    
    public function create(array $values = array()) {
      global $user;
      $values += array(
        'title' => '',
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'uid' => $user->uid,
        'oid' => '',
      );
      return parent::create($values);
    }
  }
  
  /**
   * UI controller for Budget Requests.
   */
  class BudgetsysRequestUIController extends EntityDefaultUIController {
    /**
     * Overrides hook_menu() defaults.
     */
    public function hook_menu() {
      $items = parent::hook_menu();
      $items[$this->path]['description'] = 'Manage Requests.';
      return $items;
    }
  }